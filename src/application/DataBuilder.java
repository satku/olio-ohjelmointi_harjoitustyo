/**
* TIMO
* DataBuilder.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/

package application;

/* Reads data from Posti's XML */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DataBuilder {
	static private DataBuilder instance = null;
	private ArrayList<SmartPost> dataBuilder_arrayList = new ArrayList<SmartPost>();
	private Document doc;

	private DataBuilder() throws IOException {
		String line;
		String content = "";

		URL url = new URL("http://smartpost.ee/fi_apt.xml");

		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

		while ((line = br.readLine()) != null) {
			content += line;
		}

		parseXml(content);

	}

	static public DataBuilder getInstance() throws IOException {
		if (instance == null) {
			instance = new DataBuilder();
		}
		return instance;
	}

	private void parseXml(String xml) {

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			doc = dBuilder.parse(new InputSource(new StringReader(xml)));

			doc.getDocumentElement().normalize();

			parseCurrentData();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	private void parseCurrentData() {
		NodeList nodes = doc.getElementsByTagName("place");

		for (int i = 0; i < nodes.getLength(); i++) {
			String postOffice = null;
			Node node = nodes.item(i);
			Element e = (Element) node;

			// Strip "Pakettiautomaatti" etc. from the postoffice string in XML
			if (getValue("postoffice", e).indexOf(",") != -1) {
				String[] office = getValue("postoffice", e).split(",");
				postOffice = office[1].trim();
			} else {
				postOffice = getValue("postoffice", e);
			}

			// Add new SmartPost with all the info to ArrayList.
			dataBuilder_arrayList.add(new SmartPost(getValue("code", e), getValue("city", e), getValue("address", e), getValue("availability", e), postOffice, Double.parseDouble(getValue("lat", e)), Double.parseDouble(getValue("lng", e))));
		}
	}

	// Gets text from XML by tag.
	private String getValue(String tag, Element e) {
		return e.getElementsByTagName(tag).item(0).getTextContent();
	}

	public ArrayList<SmartPost> getSmartPost() {
		return dataBuilder_arrayList;
	}

}
