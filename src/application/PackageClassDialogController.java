/**
* TIMO
* PackageClassDialogController.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/

package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class PackageClassDialogController {
	@FXML
	private Label infoLabel;
	@FXML
	private Button closeButton;

	String text = "Valitse pakettiluokka seuraavalla tavalla:\n\n1. luokka:\n\tMitat: korkeintaan 19"
			+ "*36*60 cm\n\tPaino korkeintaan 18 kg\n\tEi särkyvää\n\t"
			+ "Etäisyys määränpäähän korkeintaan 150 km\n\n2. luokka:\n\tMitat korkeintaan 11*"
			+ "36*60 cm\n\tPaino korkeintaan 2 kg\n\tVoi olla särkyvää\n\n"
			+ "3. luokka:\n\tLeveys korkeintaan 59*36*60 cm\n\tPaino "
			+ "korkeintaan 35 kg\n\tEi särkyvää";

	@FXML
	void handleButtonClick(ActionEvent e) {
		((Node) e.getSource()).getScene().getWindow().hide(); // Close window.
	}

	@FXML
	private void initialize() {
		infoLabel.setText(text);
	}

}
