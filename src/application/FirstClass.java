package application;

/**
* TIMO
* FirstClass.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/
import java.util.ArrayList;

public class FirstClass extends Package{
	public FirstClass(String product, double[] dimensions, double weight, ArrayList<SmartPost> startAndDestination) {
		super(product, 1, dimensions, weight,startAndDestination);
	}

}
