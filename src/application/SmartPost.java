package application;
/**
* TIMO
* SmartPost.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/

public class SmartPost {
	private String code;
	private String city;
	private String address;
	private String availability;
	private String postOffice;
	private GeoPoint geoPoint = null;

	public SmartPost(String id, String town, String street, String openTime, String office, double latitude, double longitude) {
		code = id;
		city = town;
		address = street;
		availability = openTime;
		postOffice = office;
		geoPoint = new GeoPoint(latitude, longitude);
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		// Conversion to String so leading zeros don't get stripped out
		return "" + code;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return the availability
	 */
	public String getAvailability() {
		return availability;
	}

	/**
	 * @return the postOffice
	 */
	public String getPostOffice() {
		return postOffice;
	}

	/**
	 * @return the lat and lng
	 */
	public double[] getLatAndLng() {
		return geoPoint.getLatAndLng();
	}

}
