package application;

/**
* TIMO
* ThirdClass.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/

import java.util.ArrayList;

public class ThirdClass extends Package{
	public ThirdClass(String product, double[] dimensions, double weight, ArrayList<SmartPost> startAndDestination) {
		super(product, 3, dimensions, weight,startAndDestination);
	}

}
