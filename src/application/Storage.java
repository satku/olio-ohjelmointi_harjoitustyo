package application;
/**
* TIMO
* Storage.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/
import java.util.ArrayList;
import java.util.Iterator;

public class Storage {
	private ArrayList<Package> storedPackages = new ArrayList<Package>();
	static private Storage storage;
	public boolean packagesLoadedFromLog = false;

	private Storage() {
	}

	static public Storage getInstance() {
		if (storage == null)
			storage = new Storage();
		return storage;
	}

	public void addPackage(Package p) {
		storedPackages.add(p);
	}

	public ArrayList<Package> getStorage() {
		return storedPackages;
	}

	public void removePackage(String product) {
		for (Iterator<Package> iterator = storedPackages.iterator(); iterator.hasNext();) {
		    if (iterator.next().product.equals(product)) {
		        iterator.remove();
		    }
		}
	}

}