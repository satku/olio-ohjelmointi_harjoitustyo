/**
* TIMO
* PackageViewController.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/

package application;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class PackageViewController {
	@FXML
	private TableView<Integer> packagesListTable;
	@FXML
	private ArrayList<Package> packages = new ArrayList<Package>();


	// On initialize, create 7 columns for the table and
	// add each SmartPost data to the columns from the Storage.
	@FXML
	private void initialize() {
		// Create Lists which will contain data from each Package.
        List<String> product = new ArrayList<String>();
        List<String> size = new ArrayList<String>();
        List<String> weight = new ArrayList<String>();
		List<Integer> packageClass = new ArrayList<Integer>();
        List<String> departure = new ArrayList<String>();
        List<String> arrival = new ArrayList<String>();
        List<String> distanceBetween = new ArrayList<String>();

		packages = Storage.getInstance().getStorage();
		for (Package p : packages) {
			// Get data from each Package.
			SmartPost spStart = p.startAndDestination.get(0);
			SmartPost spDest = p.startAndDestination.get(1);
			double distance = GeoPoint.getDistance(p.startAndDestination);
			double[] dimensions = p.dimensions;

			// Add data to each List from each Package.
			product.add(p.product);
			size.add(dimensions[0] + "x" + dimensions[1] + "x" + dimensions[2]);
			weight.add(String.valueOf(p.weight));
			packageClass.add(p.packageClass);
			departure.add(spStart.getCity() + ", " + spStart.getPostOffice());
			arrival.add(spDest.getCity() + ", " + spDest.getPostOffice());
			distanceBetween.add(String.valueOf(distance));
		}

        for (int i = 0; i < packageClass.size() && i < product.size(); i++) {
            packagesListTable.getItems().add(i);
        }

        // Create the columns and add each List to each Column.
        TableColumn<Integer, String> productColumn = new TableColumn<>("Tuote");
        productColumn.setCellValueFactory(cellData -> {
            Integer rowIndex = cellData.getValue();
            return new ReadOnlyStringWrapper(product.get(rowIndex));
        });

        TableColumn<Integer, String> sizeColumn = new TableColumn<>("Koko [cm]");
        sizeColumn.setCellValueFactory(cellData -> {
            Integer rowIndex = cellData.getValue();
            return new ReadOnlyStringWrapper(size.get(rowIndex));
        });

        TableColumn<Integer, String> weightColumn = new TableColumn<>("Paino [kg]");
        weightColumn.setCellValueFactory(cellData -> {
            Integer rowIndex = cellData.getValue();
            return new ReadOnlyStringWrapper(weight.get(rowIndex));
        });

        TableColumn<Integer, Number> packetClassColumn = new TableColumn<>("Pakettiluokka");
        packetClassColumn.setCellValueFactory(cellData -> {
            Integer rowIndex = cellData.getValue();
            return new ReadOnlyIntegerWrapper(packageClass.get(rowIndex));
        });

        TableColumn<Integer, String> departureColumn = new TableColumn<>("Lähtöpaikka");
        departureColumn.setCellValueFactory(cellData -> {
            Integer rowIndex = cellData.getValue();
            return new ReadOnlyStringWrapper(departure.get(rowIndex));
        });

        TableColumn<Integer, String> arrivalColumn = new TableColumn<>("Saapumispaikka");
        arrivalColumn.setCellValueFactory(cellData -> {
            Integer rowIndex = cellData.getValue();
            return new ReadOnlyStringWrapper(arrival.get(rowIndex));
        });

        TableColumn<Integer, String> distanceColumn = new TableColumn<>("Etäisyys [km]");
        distanceColumn.setCellValueFactory(cellData -> {
            Integer rowIndex = cellData.getValue();
            return new ReadOnlyStringWrapper(distanceBetween.get(rowIndex));
        });

        // Add the columns to the table.
        packagesListTable.getColumns().add(productColumn);
        packagesListTable.getColumns().add(sizeColumn);
        packagesListTable.getColumns().add(weightColumn);
        packagesListTable.getColumns().add(packetClassColumn);
        packagesListTable.getColumns().add(departureColumn);
        packagesListTable.getColumns().add(arrivalColumn);
        packagesListTable.getColumns().add(distanceColumn);
	}

}